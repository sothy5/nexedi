import operator

dprg_dict={}
ncd_dict={}


def single_step_money(c,d,dprg_list, withoutEnd):
    total=c
    p= int(dprg_list[1])
    if p <= c:
        if withoutEnd:
            profit=((d-int(dprg_list[0]))*int(dprg_list[3]))-(p-int(dprg_list[2]))
        else:
            profit=((d-int(dprg_list[0])-1)*int(dprg_list[3]))-(p-int(dprg_list[2]))
        
        if profit > 0:
            total=c+profit
    
    return total    

def two_step_money(c, d, dprg_list_first, dprg_list_second):
    first_total=single_step_money(c,int(dprg_list_second[0]),dprg_list_first,False )
    second_total=single_step_money(first_total, d,dprg_list_second, True  )
    return second_total
    

def three_step_money(c, d, dprg_list_first, dprg_list_second, dprg_list_third):
    new_c=two_step_money(c, int(dprg_list_third[0]), dprg_list_first, dprg_list_second)
    if new_c >= int(dprg_list_third[1]):
       new_c=new_c-int(dprg_list_third[1])+(int(dprg_list_third[3])*(d-int(dprg_list_third[0])))+int(dprg_list_third[2])
    return new_c
      

def sortFirst(val): 
    return val[0]  

def calculate_max_money():
   i=1    
   for key in  ncd_dict:
        ncd_list=ncd_dict[key]
        n=int(ncd_list[0])
        c=int(ncd_list[1])
        d=int(ncd_list[2])
        if n==0 and c >0:
            print("Case", i,":", c)
            i=i+1
        elif n==0  and c==0:
            return         
        else:
            dprg_lists=dprg_dict[key]
            dprg_lists.sort(key=sortFirst)
            single_step_money_dict={}
            for li in dprg_lists:
                single_step_money_dict[li[0]]=single_step_money(c,d,li,True)        

            max_key=max(single_step_money_dict.items(), key=operator.itemgetter(1))[0]
            max_list_in_single_step=[]
            if n==1:
                 print("Case", i,":", single_step_money_dict[max_key])
                 i=i+1  
                 continue
            
            if n==2:
                li1=dprg_lists[0]
                li2=dprg_lists[0]
               
                if int(li1[0])== int(li2[0]):
                    print("Case", i,":", single_step_money_dict[max_key])
                    i =i+1     

            two_step_money_dict={}
            for li in dprg_lists:
                if int(li[0])==int(max_key):
                    max_list_in_single_step=li
                if int(li[0])> int(max_key):
                    money=two_step_money(c,d,max_list_in_single_step,li)
                    two_step_money_dict[li[0]]= money
	              
         
            if not bool(two_step_money_dict):
                continue 

            second_max_key=max(two_step_money_dict.items(), key=operator.itemgetter(1))[0]
            max_list_in_second_step=[]
            
            if n==2:
                if int(single_step_money_dict[max_key]) <  int(two_step_money_dict[second_max_key]):
                   print("Case ", i,":", two_step_money_dict[second_max_key])
                else:
                   print("Case ", i,":", single_step_money_dict[max_key])
                i=i+1  
                continue

            three_step_money_dict={}
            if int(single_step_money_dict[max_key]) <  int(two_step_money_dict[second_max_key]):
                for li in dprg_lists:
                    if int(li[0])==int(second_max_key):
                        max_list_in_second_step=li
                    if int(li[0])> int(second_max_key):
	                    three_step_money_dict[li[0]]=three_step_money(c,d,max_list_in_single_step,max_list_in_second_step,li)
                print("Case", i,":", two_step_money_dict[second_max_key])
                #TODO
            else: 
                 print("Case", i,":", single_step_money_dict[max_key])
            i=i+1
         



def read_data():
    f=open("input.txt", "r")
    f1=f.readlines()
    index=1
    dprg_list=[]
    
    for x in f1: 
        init_list=x.split()
        l=len(init_list)
        if l==3:
            if index==1:
                 ncd_dict[index]=init_list
                 index=index+1
            else: 
                dprg_dict[index-1]=dprg_list
                dprg_list=[]
                ncd_dict[index]=init_list
                index=index+1
        elif l==4:  
          dprg_list.append(init_list)

           
    f.close()   

def main():
    read_data()
    calculate_max_money()

if __name__ == '__main__':
    main()   

